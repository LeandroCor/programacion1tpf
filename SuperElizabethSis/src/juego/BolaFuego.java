package juego;
import java.awt.Color;

import entorno. Entorno;

public class BolaFuego {
	private double x;
	private double y;
	private int alto;
	private int ancho;
	private int diametro;
	private int velocidad;
	private boolean isUsable;
	
	BolaFuego(double x, double y, int diametro){
		this.x=x;
		this.y=y;
		this.alto = 10;
		this.ancho = 10;
		this.diametro=diametro;
		this.velocidad=3;
		this.isUsable = true;
	}
	
	
	public void moverDerecha() {
		this.x=this.x+velocidad;
	}

	
	public void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, Color.RED);
	}
	
	public boolean getStatus() {
		return isUsable;
	}
	
	public void setStatus(boolean status) {
		this.isUsable = status;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public void setVelocidad(int v) {
		this.velocidad = v;
	}
	
	public boolean colisionaCon(Soldado d) {
		if (d.getY() >= this.y-20 - (this.alto / 2) && 
			d.getY() <= this.y + (this.alto / 2) &&
			d.getX() >= this.x - (this.ancho / 2) && 
			d.getX() <= this.x + (this.ancho / 2))
			return true;
		else 
			return false;
	}

	
	
	
	
}