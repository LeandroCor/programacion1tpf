package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Princesa eli;
	private Obstaculo[] piedras;
	private BolaFuego poder;							//falta modificar los carteles de puntaje y vida.
	private Soldado[] soldado;
	
	int puntaje = 0;
	int TiempoDeSalto; 
	
	Image imgFondo;
	Image perdiste;
	Image ganaste;
	Image suelo;
	// Variables y m�todos propios de cada grupo
	// ...
	
	Juego()
	{ 
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 16 - v1", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		// ...
		this.eli = new Princesa(50,555,60,20, 0);
		
		this.piedras = new Obstaculo[3];
		this.piedras[0] = new Obstaculo(800,560,50,50);
		this.piedras[1] = new Obstaculo(1300,560,50,50);
		this.piedras[2] = new Obstaculo(1800,560,50,50);
		
		this.poder = new BolaFuego(0,0,0);
		this.soldado = new Soldado[3];
		
		this.soldado[0] = new Soldado(1000, 555, 60, 20);
		this.soldado[1] = new Soldado(1500, 555, 60, 20);
		this.soldado[2] = new Soldado(2000, 555, 60, 20);
		
		suelo = Herramientas.cargarImagen("suelo.png");
		imgFondo = Herramientas.cargarImagen("fondo.png");
		perdiste = Herramientas.cargarImagen("gamerover123.png");
		ganaste = Herramientas.cargarImagen("winner123.png");
		// Inicia el juego!
		this.entorno.iniciar(); 
	} 

	/**
	 * Durante el juego, el m�todo tick() ser� ejecutado en cada instante y 
	 * por lo tanto es el m�todo m�s importante de esta clase. Aqu� se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{
		// Procesamiento de un instante de tiempo
		// ...
		
		entorno.dibujarImagen(imgFondo, 400, 299, 0);
		entorno.dibujarImagen(suelo,400,615,0); 
		dibujarPrincesa();
		
		dibujarPuntaje();
		
		dibujarVidas();
		
		dibujarSoldados();
		movimientoSoldados();
		
		dibujarPiedras();
		movimientoPiedras();
		
		dificultad();

		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if(Perder()==false ){
			
			disparar();
			
			MovimientosPersonaje();
			
			SaltodePersonaje();
			
			
			
			
		}
		Ganar();
	
			 
			
	}
	
	
	public void dibujarVidas() {
		entorno.cambiarFont("Verdana", 24, Color.red);
		this.entorno.escribirTexto("Vidas: " + eli.getVidas(), 100, 30);
	}
	
	public void dibujarPuntaje() {
		entorno.cambiarFont("Verdana", 24, Color.red);
		this.entorno.escribirTexto("Puntaje: " + puntaje, 600, 30);
		
	}
	
	public void movimientoPiedras() {
		this.piedras[0].moverIzquierda();
		this.piedras[1].moverIzquierda();
		this.piedras[2].moverIzquierda(); 
		
		if((this.piedras[0].getX()<-20)&&(this.piedras[1].getX()<-20)&&(this.piedras[2].getX()<-20)) {
			this.piedras[0] = new Obstaculo(800,560,50,50);
			this.piedras[1] = new Obstaculo(1300,560,50,50);
			this.piedras[2] = new Obstaculo(1800,560,50,50);
		}
		
	}
	
	
	
	public void dibujarPiedras(){
		this.piedras[0].dibujar(this.entorno);
		this.piedras[1].dibujar(this.entorno);
		this.piedras[2].dibujar(this.entorno);
		
		
		
	}
	
	public void movimientoSoldados() {
		this.soldado[0].moverIzquierda();
		this.soldado[1].moverIzquierda();
		this.soldado[2].moverIzquierda();
		
		if(
			this.soldado[0].getX()<-20 &&
		    this.soldado[1].getX()<-20 &&
		    this.soldado[2].getX()<-20
		   ){
			this.soldado[0] = new Soldado(1000, 555, 60, 20);
			this.soldado[1] = new Soldado(1500, 555, 60, 20);
			this.soldado[2] = new Soldado(2000, 555, 60, 20);
		}
		
		
		
	}
	
	public void dibujarPrincesa() {
		this.eli.dibujar(this.entorno);
	}
	
	
	public void dibujarSoldados() {
		this.soldado[0].dibujar(this.entorno);
		this.soldado[1].dibujar(this.entorno);
		this.soldado[2].dibujar(this.entorno);
	}
	
	public void MovimientosPersonaje() {
		if((this.entorno.estaPresionada(this.entorno.TECLA_DERECHA))&&(this.eli.getX()<this.entorno.ancho()-500)) {
			this.eli.moverDerecha();
		}
		if((this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA))&&(this.eli.getX()>10)) {
			this.eli.moverIzquierda();
		}
	}
	
	public void Ganar() 
    {
		if (puntaje>=60){
			
			entorno.dibujarImagen(ganaste, 400, 300, 0);
			entorno.cambiarFont("Verdana", 12, Color.white);
			entorno.escribirTexto("GANASTE!,Presiona Enter para salir",300,360);
			
			for(int i = 0; i < piedras.length; i++) {
				piedras[i].setVelocidad(0);
			}
			
			for(int i = 0; i < soldado.length; i++) {
				soldado[i].setVelocidad(0);
			}
			
			this.eli.setVelocidad(0);
			
			this.poder.setX(-50);
			this.poder.setY(-50);
			
			if (entorno.estaPresionada(entorno.TECLA_ENTER)){
				System.exit(0); 
			}
			
		}
		
		   
		  
		
    }	
	
	public boolean Perder() {
		for (Soldado soldadito : soldado) {
			 if (eli.colisionaCon(soldadito)) {
				 soldadito.setX(soldadito.getX() - 20);
				 eli.restarVida();
		    }
		}   
		
		for(int i = 0; i < piedras.length; i++) {
			if(this.eli.colisionaCon(piedras[i])) {
				piedras[i].setX(piedras[i].getX() - 70);
				eli.restarVida();
			}
		}
		
		if(eli.getVidas() <= 0) {
			entorno.dibujarImagen(perdiste,400,300,0);
			entorno.cambiarFont("Times New Roman", 20, Color.RED);
			entorno.escribirTexto("PERDISTE!,Presiona Enter para salir",270, 410);
			
			for(int i = 0; i < soldado.length; i++) {
				soldado[i].setVelocidad(0);
			}
			
			for(int i = 0; i < piedras.length; i++) {
				piedras[i].setVelocidad(0);
			}
			
			eli.setVelocidad(0);
			
			
			
			if (entorno.estaPresionada(entorno.TECLA_ENTER)){
				System.exit(0); 
			}
		    return true;
		}
		else {
			entorno.cambiarFont("Verdana", 24, Color.red);
			this.entorno.escribirTexto("Vidas: " + eli.getVidas(), 100, 30);
			return false;
		}
		
		
	}
	
	public void SaltodePersonaje() {
		if(entorno.sePresiono(entorno.TECLA_ARRIBA)) {
			TiempoDeSalto = 35;
			eli.setSalto();
		}
		
		if (TiempoDeSalto!=0) {
			TiempoDeSalto--;	
			eli.checkeoTiempodeSalto(TiempoDeSalto);	
		}	
	}
	
	
	public void disparar() {
		this.poder.dibujar(this.entorno);
		if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && poder.getStatus()) {
			this.poder = new BolaFuego(this.eli.getX(),this.eli.getY(),25);
			poder.setStatus(false);
			
		}
		this.poder.moverDerecha();
		
		if(this.poder.getX() >= 799) {
			poder.setStatus(true);
		}
		
		for(int i=0; i < soldado.length; i++) {
			if(poder.colisionaCon(soldado[i])) {
				poder.setStatus(true);
				puntaje += 5;
				this.entorno.escribirTexto("Puntaje: " + puntaje, 600, 30);
				soldado[i].setX(-20);
				soldado[i].setY(-20);
				poder.setX(-20);
				poder.setY(-20);
				
			}
		}
		
		
	}
	
	public void dificultad() {
		
		if(puntaje>=15) {
			for(int i=0;i<this.soldado.length;i++) {
				this.soldado[i].setVelocidad(4);
				this.piedras[i].setVelocidad(4);
			}
		}
		if(puntaje>=30) {
			for(int i=0;i<this.soldado.length;i++) {
				this.soldado[i].setVelocidad(6);
				this.piedras[i].setVelocidad(6);
			}
			
		}
		if(puntaje>=45) {
			for(int i=0;i<this.soldado.length;i++) {
				this.soldado[i].setVelocidad(8);
				this.piedras[i].setVelocidad(8);
			}
		}
		
	} 
	


	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}