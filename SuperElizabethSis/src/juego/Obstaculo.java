package juego;



import java.awt.Color;

import entorno.Entorno;

public class Obstaculo {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad;
	
	
	
	Obstaculo(int x, int y, int alto, int ancho){
		this.x=x;
		this.y=y;
		this.alto=alto;
		this.ancho=ancho;
		this.velocidad=3;
	}
		
	public void moverIzquierda() {
		this.x=this.x-velocidad;
		
	}
	
	public void setVelocidad(int v) {
		this.velocidad = v;
	}
	

	public int getVelocidad() {
		return velocidad;
	}

	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.BLUE);
		
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public boolean colisionaCon(Princesa p) {
		if (p.getY() >= this.y-20 - (this.alto / 2) && 
			p.getY() <= this.y + (this.alto / 2) &&
			p.getX() >= this.x - (this.ancho / 2) && 
			p.getX() <= this.x + (this.ancho / 2))
			return true;
		else 
			return false;
	}
	
	
}