package juego;
import java.awt.Color;

import entorno.Entorno;

public class Princesa {
	private int x;
	private int y; 
	private int alto;
	private int ancho;
	private int velocidad; 
	private int vidas;
	private int salto;
	
	Princesa(int x, int y, int alto, int ancho, int salto){
		this.x=x;
		this.y=y;
		this.alto=alto;
		this.ancho=ancho;
		this.velocidad=2;
		this.salto = salto;
		this.vidas = 3;
	}
	
	public void moverDerecha() {
		this.x=this.x+velocidad;
	}
	
	public void moverIzquierda() {
		this.x=this.x-velocidad;
	}
	
	public void setVelocidad(int v) {
		this.velocidad = v;
	}
	
	//--------------------------------------------//
	
	public void restarVida() {
		this.vidas = this.vidas - 1;
	}
	
	public int getVidas() {
		return vidas;
	} 
	
	public void setSalto() {
		this.salto = this.y; 
		this.y = 480;
	
	}
	
	public void checkeoTiempodeSalto(int tiempo){ // chequea si el personaje ya salto, de ser asi, reinicia el valor y en 0 con un metodo ReinicioY()
		if (tiempo == 0){
			ReinicioY();
		}
	}
	
	public void ReinicioY() { 
		this.y = 555;
		
	}	
	
	public boolean colisionaCon(Soldado d) {
		if (d.getY() >= this.y-20 - (this.alto / 2) && 
			d.getY() <= this.y + (this.alto / 2) &&
			d.getX() >= this.x - (this.ancho / 2) && 
			d.getX() <= this.x + (this.ancho / 2)
		){
			return true;
		}
		
		return false;	
	}
	
	
	public boolean colisionaCon(Obstaculo d) {
		if (d.getY() >= this.y - (this.alto / 2) && 
			d.getY() <= this.y + (this.alto / 2) &&
			d.getX() >= this.x -25 - (this.ancho / 2) && 
			d.getX() <= this.x +25 + (this.ancho / 2)
		) {
			return true;
		}
		return false;	
	}
	
	//------------------------------------------//
	
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.PINK);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	
	
	
	
	
	
	
	

}
