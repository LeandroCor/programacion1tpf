package juego;

import java.awt.Color;

import entorno.Entorno;

public class Soldado {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad; 
	
	Soldado(int x, int y, int alto, int ancho){
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.velocidad = 3;
	}
	
	
	public void setVelocidad(int v) {
		this.velocidad = v;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void moverIzquierda() {
		this.x=this.x-velocidad;
	}
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.ORANGE);
	}


	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	// VERIFICA SI COLICIONA LA PRINCESA CON SOLDADO
	public boolean colisionaCon(Princesa s) 
	{
		if (s.getY() >= this.y-20 - (this.alto / 2) && 
			s.getY() <= this.y + (this.alto / 2) &&
			s.getX() >= this.x - (this.ancho / 2) &&
			s.getX() <= this.x + (this.ancho / 2))
		{
			return true;
		}
		
		return false;			
	}
	
}